type ID = string | number;

interface Item {
	id: ID;
	parent: ID;
	type?: string | null;
}

export default class TreeStore {
	readonly treeMap: Map<ID, Item> = new Map();
	readonly childrenMap: Map<ID, Item[]> = new Map();
	constructor(readonly itemList: Item[]) {
		for (let item of this.itemList) {
			this.treeMap.set(item.id, item);

			if (!this.childrenMap.has(item.parent)) {
				this.childrenMap.set(item.parent, []);
			}
			const childrens = this.childrenMap.get(item.parent);
			childrens!.push(item);

			const parentItem = this.treeMap.get(item.parent);
			if (parentItem) {
			}
		}
	}

	getAll() {
		return this.itemList;
	}

	getItem(id: ID) {
		return this.treeMap.get(id);
	}

	getChildren(id: ID) {
		const item = this.treeMap.get(id);
		if (!item) {
			throw new Error(`No item with id: ${id}`);
		}
		const childrens = this.childrenMap.get(id);
		if (!childrens) {
			return [];
		}
		return Array.from(childrens);
	}

	getAllChildren(id: ID) {
		const allChildren: Item[] = [];
		let childrens = this.getChildren(id);
		while (childrens.length) {
			const children = childrens.shift();
			const newChildrens = this.getChildren(children!.id);
			childrens.push(...newChildrens);
			allChildren.push(children!);
		}
		return allChildren;
	}

	getAllParents(id: ID) {
		const allParents: Item[] = [];
		const item = this.treeMap.get(id);
		if (!item) {
			throw new Error(`No item with id: ${id}`);
		}
		let parent = this.treeMap.get(item.parent);
		while (parent) {
			allParents.push(parent);
			parent = this.treeMap.get(parent.parent);
		}
		return allParents;
	}
}

export type { ID, Item };
