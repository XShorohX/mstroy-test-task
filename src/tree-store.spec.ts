import TreeStore, { Item } from "./tree-store";

const mockItemList: Item[] = [
	{ id: 1, parent: "root" },
	{ id: 2, parent: 1, type: "test" },
	{ id: 3, parent: 1, type: "test" },

	{ id: 4, parent: 2, type: "test" },
	{ id: 5, parent: 2, type: "test" },
	{ id: 6, parent: 2, type: "test" },

	{ id: 7, parent: 4, type: null },
	{ id: 8, parent: 4, type: null },
];

describe("TreeStore", () => {
	test("getAll should return original item list", () => {
		const ts = new TreeStore(mockItemList);
		const mockAll: Item[] = [
			{ id: 1, parent: "root" },
			{ id: 2, parent: 1, type: "test" },
			{ id: 3, parent: 1, type: "test" },
			{ id: 4, parent: 2, type: "test" },
			{ id: 5, parent: 2, type: "test" },
			{ id: 6, parent: 2, type: "test" },
			{ id: 7, parent: 4, type: null },
			{ id: 8, parent: 4, type: null },
		];

		expect(ts.getAll()).toEqual(mockAll);
	});

	test("getItem should return item with id = 7", () => {
		const ts = new TreeStore(mockItemList);
		const mockItem: Item = { id: 7, parent: 4, type: null };

		expect(ts.getItem(7)).toEqual(mockItem);
	});

	test("getChildren should return childrens of item with id = 4", () => {
		const ts = new TreeStore(mockItemList);
		const mockChildrens: Item[] = [
			{ id: 7, parent: 4, type: null },
			{ id: 8, parent: 4, type: null },
		];

		expect(ts.getChildren(4)).toEqual(mockChildrens);
	});

	test("getChildren should return empty children array", () => {
		const ts = new TreeStore(mockItemList);

		expect(ts.getChildren(5)).toEqual([]);
	});

	test("getChildren should return childrens of item with id = 2", () => {
		const ts = new TreeStore(mockItemList);
		const mockChildrens = [
			{ id: 4, parent: 2, type: "test" },
			{ id: 5, parent: 2, type: "test" },
			{ id: 6, parent: 2, type: "test" },
		];

		expect(ts.getChildren(2)).toEqual(mockChildrens);
	});

	test("getChildren should throw error", () => {
		const ts = new TreeStore(mockItemList);

		try {
			ts.getChildren(-1);
			new Error("Should throw error");
		} catch (e) {
			expect(e).toEqual(new Error("No item with id: -1"));
		}
	});

	test("getAllChildren should return all childrens of item with id = 2", () => {
		const ts = new TreeStore(mockItemList);
		const mockAllChildrens = [
			{ id: 4, parent: 2, type: "test" },
			{ id: 5, parent: 2, type: "test" },
			{ id: 6, parent: 2, type: "test" },
			{ id: 7, parent: 4, type: null },
			{ id: 8, parent: 4, type: null },
		];

		expect(ts.getAllChildren(2)).toEqual(mockAllChildrens);
	});

	test("getAllParents should return all parents of item with id = 7", () => {
		const ts = new TreeStore(mockItemList);
		const mockAllParents = [
			{ id: 4, parent: 2, type: "test" },
			{ id: 2, parent: 1, type: "test" },
			{ id: 1, parent: "root" },
		];

		expect(ts.getAllParents(7)).toEqual(mockAllParents);
	});

	test("getAllParents should throw error", () => {
		const ts = new TreeStore(mockItemList);

		try {
			ts.getAllParents(-1);
			new Error("Should throw error");
		} catch (e) {
			expect(e).toEqual(new Error("No item with id: -1"));
		}
	});

	test("methods should not mutate original item list", () => {
		const mockItemList: Item[] = [
			{ id: "aaa", parent: "root" },
			{ id: "ccc", parent: "bbb", type: "test" },
			{ id: "bbb", parent: "aaa", type: "test" },

			{ id: 1, parent: "aaa", type: "test" },
			{ id: 3, parent: "aaa", type: "test" },
			{ id: 2, parent: "aaa", type: "test" },

			{ id: "1", parent: "bbb", type: "test" },
			{ id: "3", parent: "bbb", type: "test" },
			{ id: "2", parent: "bbb", type: "test" },

			{ id: "ffff", parent: "2", type: null },
			{ id: "bbbb", parent: "ffff", type: null },
		];

		const ts = new TreeStore(mockItemList);

		const mockAll: Item[] = [
			{ id: "aaa", parent: "root" },
			{ id: "ccc", parent: "bbb", type: "test" },
			{ id: "bbb", parent: "aaa", type: "test" },

			{ id: 1, parent: "aaa", type: "test" },
			{ id: 3, parent: "aaa", type: "test" },
			{ id: 2, parent: "aaa", type: "test" },

			{ id: "1", parent: "bbb", type: "test" },
			{ id: "3", parent: "bbb", type: "test" },
			{ id: "2", parent: "bbb", type: "test" },

			{ id: "ffff", parent: "2", type: null },
			{ id: "bbbb", parent: "ffff", type: null },
		];

		expect(ts.getAll()).toEqual(mockAll);

		const mockItem: Item = { id: "1", parent: "bbb", type: "test" };

		expect(ts.getItem("1")).toEqual(mockItem);
		expect(ts.getItem("1")).toEqual(mockItem);

		const mockChildrens: Item[] = [
			{ id: "ccc", parent: "bbb", type: "test" },
			{ id: "1", parent: "bbb", type: "test" },
			{ id: "3", parent: "bbb", type: "test" },
			{ id: "2", parent: "bbb", type: "test" },
		];

		expect(ts.getChildren("bbb")).toEqual(mockChildrens);
		expect(ts.getChildren("bbb")).toEqual(mockChildrens);

		const mockAllChildrens = [
			{ id: "ccc", parent: "bbb", type: "test" },
			{ id: "1", parent: "bbb", type: "test" },
			{ id: "3", parent: "bbb", type: "test" },
			{ id: "2", parent: "bbb", type: "test" },
			{ id: "ffff", parent: "2", type: null },
			{ id: "bbbb", parent: "ffff", type: null },
		];

		expect(ts.getAllChildren("bbb")).toEqual(mockAllChildrens);
		expect(ts.getAllChildren("bbb")).toEqual(mockAllChildrens);

		const mockAllParents = [
			{ id: "ffff", parent: "2", type: null },
			{ id: "2", parent: "bbb", type: "test" },
			{ id: "bbb", parent: "aaa", type: "test" },
			{ id: "aaa", parent: "root" },
		];

		expect(ts.getAllParents("bbbb")).toEqual(mockAllParents);
		expect(ts.getAllParents("bbbb")).toEqual(mockAllParents);

		expect(ts.itemList).toEqual(mockItemList);
	});
});
